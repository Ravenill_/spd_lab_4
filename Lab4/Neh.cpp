#include "Neh.h"
#include <algorithm>
#include "DunnoSort.h"


Neh::Neh()
{
}


Neh::~Neh()
{
}

void Neh::sort(std::vector<std::vector<Task>>& taskVector)
{
    int count_of_machines = taskVector[0].size();
    int count_of_tasks = taskVector.size();

    /*
    std::vector<int> time_for_one_task;

    for (int i = 0; i < count_of_tasks; i++)
    {
        time_for_one_task.push_back(0);
    }

    int i = 0;
    for (auto& machine : taskVector)
    {
        for (auto& task : machine)
        {
            time_for_one_task[i] = time_for_one_task[i] + task.time;
        }
        i++;
    }
    */

    std::sort(taskVector.begin(), taskVector.end(), [](std::vector<Task>& a, std::vector<Task>& b) 
    {
        int a_sum = 0;
        int b_sum = 0;

        for (auto& task : a)
            a_sum = a_sum + task.time;

        for (auto& task : b)
            b_sum = b_sum + task.time;

        return a_sum > b_sum;
    });

    int temp_best_position = 999999999;
    int best_cmax = 99999999;
    for (int j = 1; j < count_of_tasks; j++)
    {
        best_cmax = TimeFinder::sort(taskVector, j).back();
        temp_best_position = j;

        std::vector<Task> temp_value = taskVector[j];
        taskVector.erase(taskVector.begin() + j);
        taskVector.insert(taskVector.begin(), temp_value);
        for (int k = 0; k < j; k++)
        {
            int cmax = TimeFinder::sort(taskVector, j).back();
            if (cmax < best_cmax)
            {
                best_cmax = cmax;
                temp_best_position = k;
            }
            std::swap(taskVector[k], taskVector[k + 1]);
        }
        taskVector.erase(taskVector.begin() + j);
        taskVector.insert(taskVector.begin() + temp_best_position, temp_value);
        /*
        //DEBUG
        std::cerr << "Insert pos: " << temp_best_position;
        for (auto& task : taskVector[j]) std::cerr << task << " ";
        std::cerr << "\n\n";
        */
    }
}

void Neh::sortMod(std::vector<std::vector<Task>>& taskVector)
{
    int count_of_machines = taskVector[0].size();
    int count_of_tasks = taskVector.size();

    sort(taskVector);

    std::sort(taskVector.begin(), taskVector.end(), [](std::vector<Task>& a, std::vector<Task>& b)
    {
        int a_sum = 0;
        int b_sum = 0;

        for (auto& task : a)
            a_sum = a_sum + task.time;

        for (auto& task : b)
            b_sum = b_sum + task.time;

        return a_sum > b_sum;
    });

    int temp_best_position = 999999999;
    int best_cmax = 99999999;
    for (int j = 1; j < count_of_tasks; j++)
    {
        best_cmax = TimeFinder::sort(taskVector, j).back();
        temp_best_position = j;

        std::vector<Task> temp_value = taskVector[j];
        taskVector.erase(taskVector.begin() + j);
        taskVector.insert(taskVector.begin(), temp_value);
        for (int k = 0; k < j; k++)
        {
            int cmax = TimeFinder::sort(taskVector, j).back();
            if (cmax < best_cmax)
            {
                best_cmax = cmax;
                temp_best_position = k;
            }
            std::swap(taskVector[k], taskVector[k + 1]);
        }
        taskVector.erase(taskVector.begin() + j);
        taskVector.insert(taskVector.begin() + temp_best_position, temp_value);
        /*
        //DEBUG
        std::cerr << "Insert pos: " << temp_best_position;
        for (auto& task : taskVector[j]) std::cerr << task << " ";
        std::cerr << "\n\n";
        */
    }

    int no_task_with_max_time = 0;
    int max_time = 0;

    for (auto& task : taskVector)
    {
        int temp_time = 0;
        for (auto& task_on_machine : task)
        {
            temp_time += task_on_machine.time;
        }

        if (temp_time > max_time)
        {
            max_time = temp_time;
            no_task_with_max_time = &task - &taskVector[0];
        }
    }

    temp_best_position = 999999999;
    best_cmax = 99999999;

    std::vector<Task> temp_value = taskVector[no_task_with_max_time];
    taskVector.erase(taskVector.begin() + no_task_with_max_time);
    taskVector.insert(taskVector.begin(), temp_value);
    for (int k = 0; k < count_of_tasks - 1; k++)
    {
        int cmax = TimeFinder::sort(taskVector).back();
        if (cmax < best_cmax)
        {
            best_cmax = cmax;
            temp_best_position = k;
        }
        std::swap(taskVector[k], taskVector[k + 1]);
    }
    int cmax = TimeFinder::sort(taskVector).back();
    if (cmax < best_cmax)
    {
        best_cmax = cmax;
        return;
    }
    taskVector.erase(taskVector.end() - 1);
    taskVector.insert(taskVector.begin() + temp_best_position, temp_value);
}
