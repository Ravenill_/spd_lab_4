#pragma once
#include "Task.h"
#include <vector>

class TimeFinder
{
public:
    TimeFinder();
    ~TimeFinder();

    static const std::vector<int> sort(std::vector<std::vector<Task>>& taskVector);
    static const std::vector<int> sort(std::vector<std::vector<Task>>& taskVector, int limit);
};

