#include "Reader.h"
#include <iostream>
#include <assert.h>

Reader::Reader(std::string fileName) : countOfTasks(0), countOfMachines(0)
{
	openFile(fileName);
}

Reader::~Reader()
{
	fileOutput.close();
}

Reader::Reader() : countOfTasks(0), countOfMachines(0)
{
	std::string fileName;
	std::cout << "Give me the name... Name of the file!\n";
	std::cin >> fileName;

	openFile(fileName);
}

void Reader::openFile(std::string fileName)
{
	fileOutput.open(fileName.c_str(), std::ios::in);
	if (!fileOutput.is_open())
	{
		std::cerr << "Warning! File not found\n";
		return;
	}
}

void Reader::readFromFileTo(std::vector<std::vector<Task>>& taskVectorStructure)
{
	fileOutput.clear();
	fileOutput.seekg(0, std::ios::beg);
	
	fileOutput >> countOfTasks;
	fileOutput >> countOfMachines;

	for (int i = 0; i < countOfTasks; i++)
	{
        std::vector<Task> temp_time_on_machine;
        for (int j = 0; j < countOfMachines; j++)
        {
            Task temp_task;
            fileOutput >> temp_task;
            temp_time_on_machine.push_back(temp_task);
            
        }
        taskVectorStructure.push_back(temp_time_on_machine);
	}
}

int Reader::getCountOfTasks() const
{
	return countOfTasks;
}

int Reader::getCountOfMachines() const
{
	return countOfMachines;
}
