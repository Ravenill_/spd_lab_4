#pragma once
#include <string>
#include <fstream>
#include <vector>
#include "Task.h"
#include <queue>

class Reader
{
private:
	int countOfTasks;
	int countOfMachines;
	std::fstream fileOutput;	

public:
	Reader(std::string fileName);
	Reader();
	~Reader();

	void openFile(std::string fileName);
    void readFromFileTo(std::vector<std::vector<Task>>& taskVectorStructure);

	int getCountOfTasks() const;
	int getCountOfMachines() const;
};