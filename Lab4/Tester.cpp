#include "Tester.h"
#include "SA.h"
#include "DunnoSort.h"
#include <memory>

Tester::Tester()
{
}


Tester::~Tester()
{
}

float Tester::test(std::vector<std::vector<Task>>& taskVector, float temperature, int amount_of_tests)
{
    std::unique_ptr<int[]> times(new int[amount_of_tests]);
    
    for (int i = 0; i < amount_of_tests; i++)
    {
        SA::sort(taskVector, 5000);
        std::vector<int> timeVect = TimeFinder::sort(taskVector);

        times[i] = timeVect[timeVect.size() - 1];
    }

    float average = 0;
    //std::cout << "\n\n";
    std::cout << "SA END TIMES: " << "\n";
    for (int i = 0; i < amount_of_tests; i++)
    {
        std::cout << times[i] << " ";
        average += times[i];
    }
    
    average = average / amount_of_tests;
    /*
    std::cout << "\n";
    std::cout << "SA AVERAGE TESTS TIME: " << average << "\n";
    */

    return average;
}
