#include "SA.h"
#include "DunnoSort.h"
#include <ctime>

SA::SA()
{

}

SA::~SA()
{

}

void SA::sort(std::vector<std::vector<Task>>& taskVector, float t)
{
    const float TEMP_MIN = 0.01;
    const float TEMP_MOD = 0.97;

    int count_of_machines = taskVector[0].size();
    int count_of_tasks = taskVector.size();

    srand(static_cast<unsigned int>(time(NULL)));

    int CMax = TimeFinder::sort(taskVector).back();

    do
    {
        std::vector<std::vector<Task>> tempTaskVector(taskVector);

        doSomething(taskVector);

        int newCMax = TimeFinder::sort(taskVector).back();
        if (newCMax >= CMax)
        {
            float rnd = static_cast<float>(std::rand()) /static_cast<float>(RAND_MAX);
            float p = exp((CMax - newCMax) / t);

            if (rnd >= p)
            {
                taskVector = tempTaskVector;
            }
        }
        else
        {
            CMax = newCMax;
        }

        t = t * TEMP_MOD;
    } while (t > TEMP_MIN);
}

void SA::doSomething(std::vector<std::vector<Task>>& taskVector)
{
    int opt = std::rand() % 3;

    switch (opt)
    {
    case 0:
        swap(taskVector);
        break;
    case 1:
        insert(taskVector);
        break;
    case 2:
        twist(taskVector);
        break;
    }
}

void SA::swap(std::vector<std::vector<Task>>& taskVector)
{
    int count_of_tasks = taskVector.size();

    int posLeft = std::rand() % count_of_tasks;
    int posRight = std::rand() % count_of_tasks;

    std::swap(taskVector[posLeft], taskVector[posRight]);
}

void SA::insert(std::vector<std::vector<Task>>& taskVector)
{
    int count_of_tasks = taskVector.size();

    int posElement = std::rand() % count_of_tasks;
    int posPut = std::rand() % (count_of_tasks - 1);

    std::vector<Task> temp_value = taskVector[posElement];
    taskVector.erase(taskVector.begin() + posElement);
    taskVector.insert(taskVector.begin() + posPut, temp_value);
}

void SA::twist(std::vector<std::vector<Task>>& taskVector)
{
    int count_of_tasks = taskVector.size();

    int posTwist = std::rand() % (count_of_tasks - 1);
    std::swap(taskVector[posTwist], taskVector[posTwist + 1]);
}