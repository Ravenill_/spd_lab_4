#include "Genetic.h"
#include "DunnoSort.h"
#include <utility>
#include <time.h>
#include <algorithm>
#include <cmath>

Genetic::Genetic()
{
}


Genetic::~Genetic()
{
}

int Genetic::sort(std::vector<std::vector<Task>>& taskVector)
{
    const int START_COUNT_OF_POPULATION = 20;
    const int AMOUNT_OF_GENERATIONS = 10;

    std::vector<std::pair<std::vector<std::vector<Task>>, std::pair<int, float>>> population;
    population.push_back(std::make_pair(taskVector, std::make_pair(TimeFinder::sort(taskVector).back(), 0)));

    //Starting generation
    std::srand(static_cast<unsigned int>(time(NULL)));
    std::vector<std::vector<Task>> generatePopulationVect(taskVector);
    for (int i = 0; i < START_COUNT_OF_POPULATION - 1; i++)
    {
        std::random_shuffle(generatePopulationVect.begin(), generatePopulationVect.end());
        population.push_back(std::make_pair(generatePopulationVect, std::make_pair(TimeFinder::sort(generatePopulationVect).back(), 0)));
    }

    for (int generations = 0; generations < AMOUNT_OF_GENERATIONS; generations++)
    {
        std::cerr << "Generation: " << generations << "\n";
        std::cerr << "Population: " << population.size() << "\n";
        
        //Selection
        float denominatorPk = 0;
        for (auto& fellow : population)
        {
            float cmax = fellow.second.first;
            denominatorPk += (1 / cmax);
        }

        for (auto& fellow : population)
        {
            float cmax = fellow.second.first;
            float generatedPk = ((1 / cmax) / denominatorPk);
            fellow.second.second = generatedPk;
        }

        std::sort(population.begin(), population.end(), [](auto& a, auto& b)
        {
            return a.second.second > b.second.second;
        });

        float sum = 0;
        int erasingIterator = 0;
        for (; erasingIterator < population.size(); erasingIterator++)
        {
            float tempPk = population[erasingIterator].second.second;
            sum += tempPk;
            if (sum > 0.7)
                break;
        }
        for (int i = 1; i < (population.size() - erasingIterator); i++)
        {
            population.pop_back();
        }

        //Crossing
        //std::vector<std::pair<std::vector<std::vector<Task>>, std::pair<int, float>>> newPopulation;
        for (int i = 0; i < (population.size() / 2); i++)
        {
            int randFellow1 = std::rand() % population.size();
            int randFellow2 = std::rand() % population.size();

            std::vector<std::vector<Task>> parent1(population[randFellow1].first);
            std::vector<std::vector<Task>> parent2(population[randFellow2].first);

            int amountOfTasks = population[0].first.size();
            int howLong1 = std::rand() % (amountOfTasks / 2);
            int howLong2 = (std::rand() % (amountOfTasks / 2)) + (amountOfTasks / 2);

            std::vector<std::vector<Task>> kidFellow(parent1);
            std::vector<Task> tempTaskVect;
            for (int j = 0; j < kidFellow[0].size(); j++)
            {
                Task temp_task;
                tempTaskVect.push_back(temp_task);
            }

            for (int j = 0; j < howLong1; j++)
            {
                kidFellow[j] = tempTaskVect;
            }
            for (int k = howLong2; k < kidFellow.size(); k++)
            {
                kidFellow[k] = tempTaskVect;
            }

            int kidIterator = 0;
            if (kidIterator == howLong1)
                kidIterator = howLong2;

            for (int j = 0; j < parent2.size(); j++)
            {              
                auto result = std::find_if(kidFellow.begin(), kidFellow.end(), [j, parent2](const std::vector<Task>& a) 
                {
                    for (int leng = 0; leng < parent2[j].size(); leng++)
                    {
                        if (a[leng].time == parent2[j][leng].time)
                        {
                            continue;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return true;
                });


                if (result == std::end(kidFellow))
                {
                    std::vector<Task> tempTaskVect(parent2[j]);
                    kidFellow.erase(kidFellow.begin() + kidIterator);
                    kidFellow.insert(kidFellow.begin() + kidIterator, tempTaskVect);
                    kidIterator++;
                    if (kidIterator == howLong1)
                        kidIterator = howLong2;
                }
                else
                {
                    continue;
                }
            }
            //newPopulation.push_back(std::make_pair(kidFellow, std::make_pair(TimeFinder::sort(kidFellow).back(), 0)));
            population.push_back(std::make_pair(kidFellow, std::make_pair(TimeFinder::sort(kidFellow).back(), 0)));
        }
        //population.clear();
        //population = newPopulation;

        //Mutation
        int howMuchMutant = 0.1 * population.size();
        for (int i = 0; i < howMuchMutant; i++)
        {
            int randFellow = std::rand() % population.size();

            int randTask1 = std::rand() % population[0].first.size();
            int randTask2 = std::rand() % population[0].first.size();

            std::swap(population[randFellow].first[randTask1], population[randFellow].first[randTask2]);
        }
    }

    int minCMax = 999999;
    for (auto& fellow : population)
    {
        int cmax = TimeFinder::sort(fellow.first).back();
        if (cmax < minCMax)
            minCMax = cmax;
    }
    return minCMax;
}
