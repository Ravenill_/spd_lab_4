#include "Task.h"

Task::Task()
: time(0)
{

}

Task& Task::operator=(const Task& arg)
{
	time = arg.time;
	return *this;
}

bool Task::operator==(const Task & a)
{
    return time == a.time;
}

std::istream& operator >> (std::istream& inputStream, Task& obj)
{
    int trash;
    inputStream >> trash;

    inputStream >> obj.time;

	return inputStream;
}

std::ostream& operator << (std::ostream & inputStream, Task & obj)
{
	std::cout << "time:" << obj.time << "\t";
	return inputStream;
}

bool operator==(const Task& lhs, const Task& rhs)
{
    return lhs.time == rhs.time;
}