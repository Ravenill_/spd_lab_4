#pragma once
#include <vector>
#include "Task.h"

class SA
{
public:
    SA();
    ~SA();

    static void sort(std::vector<std::vector<Task>>& taskVector, float t);

private:
    static void doSomething(std::vector<std::vector<Task>>& taskVector);
    static void swap(std::vector<std::vector<Task>>& taskVector);
    static void insert(std::vector<std::vector<Task>>& taskVector);
    static void twist(std::vector<std::vector<Task>>& taskVector);
};

