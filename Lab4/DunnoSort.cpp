#include "DunnoSort.h"

TimeFinder::TimeFinder()
{

}

TimeFinder::~TimeFinder()
{

}

const std::vector<int> TimeFinder::sort(std::vector<std::vector<Task>>& taskVector)
{
    std::vector<int> time_vector;
    int count_of_machines = taskVector[0].size();
    int count_of_tasks = taskVector.size();
    
    for (int i = 0; i < count_of_machines; i++)
    {
        time_vector.push_back(0);
    }

    time_vector[0] = taskVector[0][0].time;
    for (int i = 1; i < count_of_machines; i++)
    {
        time_vector[i] = taskVector[0][i].time + time_vector[i - 1];
    }

    for (int i = 1; i < count_of_tasks; i++)
    {
        time_vector[0] += taskVector[i][0].time;
        for (int j = 1; j < count_of_machines; j++)
        {
            if (time_vector[j - 1] > time_vector[j])
            {
                time_vector[j] = time_vector[j-1] + taskVector[i][j].time;
            }
            else
            {
                time_vector[j] = time_vector[j] + taskVector[i][j].time;
            }
        }
    }

    return time_vector;
}

const std::vector<int> TimeFinder::sort(std::vector<std::vector<Task>>& taskVector, int limit)
{
    std::vector<int> time_vector;
    int count_of_machines = taskVector[0].size();
    int count_of_tasks = taskVector.size();

    for (int i = 0; i < count_of_machines; i++)
    {
        time_vector.push_back(0);
    }

    time_vector[0] = taskVector[0][0].time;
    for (int i = 1; i < count_of_machines; i++)
    {
        time_vector[i] = taskVector[0][i].time + time_vector[i - 1];
    }

    for (int i = 1; i <= limit; i++)
    {
        time_vector[0] += taskVector[i][0].time;
        for (int j = 1; j < count_of_machines; j++)
        {
            if (time_vector[j - 1] > time_vector[j])
            {
                time_vector[j] = time_vector[j - 1] + taskVector[i][j].time;
            }
            else
            {
                time_vector[j] = time_vector[j] + taskVector[i][j].time;
            }
        }
    }

    return time_vector;
}