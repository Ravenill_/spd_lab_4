#include "TestModule.h"
#include <string>
#include <iostream>
#include <vector>
#include "Task.h"
#include "Reader.h"
#include "Neh.h"
#include "SA.h"
#include "DunnoSort.h"
#include "Tester.h"
#include "pomiar.h"

TestModule::TestModule()
{
    createFile("wyniki.txt");
    start();
}


TestModule::~TestModule()
{
    fileOutput.close();
}

void TestModule::start()
{
    const float TEMP = 5000;
    const int AMOUNT_OF_SA_TESTS = 10;
    
    std::vector<std::vector<Task>> taskVector;

    int cMaxClear = 0;
    int cMaxNeh = 0;
    int cMaxNehMod = 0;
    int cMaxSA = 0;

    double timeNeh = 0;
    double timeNehMod = 0;
    double timeSA = 0;

    LARGE_INTEGER performanceCountStart, performanceCountEnd;
    __int64 freq;
    QueryPerformanceFrequency((LARGE_INTEGER *)&freq);

    for (int i = 0; i < 200; i++)
    {       
        taskVector.clear();
        std::string tempFileName = "pliki/";
        tempFileName += (std::to_string(i) + ".txt");
        Reader reader(tempFileName);
        std::cout << tempFileName << "\n";

        //clear
        reader.readFromFileTo(taskVector);
        int count_of_machines = taskVector[0].size();
        int count_of_tasks = taskVector.size();
        std::cout << "Count of machines: " << count_of_machines << "\n";
        std::cout << "Count of tasks: " << count_of_tasks << "\n";
        cMaxClear = TimeFinder::sort(taskVector).back();
        std::cout << "Succesful loaded clear data...\nClear CMax: " << cMaxClear << "\n";

        //Neh
        performanceCountStart = startTimer();
        Neh::sort(taskVector);
        performanceCountEnd = endTimer();
        timeNeh = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
        cMaxNeh = TimeFinder::sort(taskVector).back();
        std::cout << "Ended Neh...\nNeh CMax: " << cMaxNeh << "  Time: " << timeNeh << "\n";
        taskVector.clear();

        //NehMod
        reader.readFromFileTo(taskVector);
        performanceCountStart = startTimer();
        Neh::sortMod(taskVector);
        performanceCountEnd = endTimer();
        timeNehMod = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
        cMaxNehMod = TimeFinder::sort(taskVector).back();
        std::cout << "Ended NehMod...\nNehMod CMax: " << cMaxNehMod << "  Time: " << timeNehMod << "\n";
        taskVector.clear();

        //SA
        reader.readFromFileTo(taskVector);
        performanceCountStart = startTimer();
        SA::sort(taskVector, TEMP);
        performanceCountEnd = endTimer();
        timeSA = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
        cMaxSA = static_cast<int>(Tester::test(taskVector, TEMP, AMOUNT_OF_SA_TESTS));
        std::cout << "Ended SA...\nSA average CMax: " << cMaxSA << "  Time for one SA: " << timeSA << "\n";

        fileOutput << tempFileName << "\n";
        fileOutput << "NON_ALG " << count_of_tasks << " " << count_of_machines << " " << cMaxClear << " 0\n";
        fileOutput << "NEH " << count_of_tasks << " " << count_of_machines << " " << cMaxNeh << " " << timeNeh << "\n";
        fileOutput << "NEH_MOD " << count_of_tasks << " " << count_of_machines << " " << cMaxNehMod << " " << timeNehMod << "\n";
        fileOutput << "SA " << count_of_tasks << " " << count_of_machines << " " << cMaxSA << " " << timeSA << "\n";
        fileOutput << "\n";
        std::cout << "\n";
    }
}

bool TestModule::createFile(std::string fileName)
{
    fileOutput.open(fileName.c_str(), std::ios::out);
    if (!fileOutput.is_open())
    {
        std::cerr << "Warning! File cannot be created\n";
        return false;
    }
    return true;
}

