#pragma once
#include <fstream>

class TestModule
{
public:
    TestModule();
    ~TestModule();

private:
    std::fstream fileOutput;

private:
    bool createFile(std::string fileName);
    void start();
};

