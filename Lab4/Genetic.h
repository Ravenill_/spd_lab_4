#pragma once
#include <vector>
#include "Task.h"

class Genetic
{
public:
    Genetic();
    ~Genetic();

    static int sort(std::vector<std::vector<Task>>& taskVector);
};

