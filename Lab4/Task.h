#pragma once
#include <iostream>

class Task
{
public:
    int time;

	Task();
	Task(int _time) : time(_time) {}

	friend std::istream& operator >> (std::istream& inputStream, Task& obj);
	friend std::ostream& operator << (std::ostream& outputStream, Task& obj);
	Task& operator =(const Task& arg);
    bool operator ==(const Task& a);
};

bool operator==(const Task& lhs, const Task& rhs);