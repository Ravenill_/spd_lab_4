#include "Reader.h"
#include "DunnoSort.h"
#include <cstdlib>
#include "Neh.h"
#include "SA.h"
#include "Tester.h"
#include "TestModule.h"
#include "Genetic.h"

/*
void printCmax()
{
	std::cout << "\n";
	std::cout << "Value of Cmax: ";
}
*/

/*
int main()
{
	std::vector<std::vector<Task>> taskVect; //wektor, kt�ry przechowuje taski
    std::vector<int> timeVect;

	Reader reader; //obiekt czytaj�cy z pliku. W konstruktorze mo�esz poda� jego nazw�.
	reader.readFromFileTo(taskVect); //fatkycznie wczytanie z pliku do wektora
	
	std::cout << "Loaded data:\n";
    for (auto& machine : taskVect)
    {
        int i = 1;
        for (auto& task : machine)
        {
            std::cout << i++ << ": " << task;
        }
    }

    timeVect = TimeFinder::sort(taskVect);

    std::cout << "\n\nTIMES:\n";
    int lp = 1;
    for (auto& time : timeVect)
    {
        std::cout << lp++ << ": " << time << "\n";
    }

    std::cout << "\n\n";
    std::cout << "NONSORT END TIME: " << timeVect[timeVect.size() - 1] << "\n";

    Neh::sort(taskVect);

    timeVect = TimeFinder::sort(taskVect);

    std::cout << "\n\nTIMES:\n";
    lp = 1;
    for (auto& time : timeVect)
    {
        std::cout << lp++ << ": " << time << "\n";
    }

    std::cout << "\n\n";
    std::cout << "NEH END TIME: " << timeVect[timeVect.size() - 1] << "\n";

    Neh::sortMod(taskVect);

    timeVect = TimeFinder::sort(taskVect);

    std::cout << "\n\nTIMES:\n";
    lp = 1;
    for (auto& time : timeVect)
    {
        std::cout << lp++ << ": " << time << "\n";
    }

    std::cout << "\n\n";
    std::cout << "NEHMOD END TIME: " << timeVect[timeVect.size() - 1] << "\n";

	system("PAUSE");
}
*/

/*
int main()
{
    std::vector<std::vector<Task>> taskVect; //wektor, kt�ry przechowuje taski
    std::vector<int> timeVect;

    Reader reader; //obiekt czytaj�cy z pliku. W konstruktorze mo�esz poda� jego nazw�.
    reader.readFromFileTo(taskVect); //fatkycznie wczytanie z pliku do wektora
    std::vector<std::vector<Task>> taskVect2(taskVect);

    std::cout << "Loaded data:\n";
    for (auto& machine : taskVect)
    {
        int i = 1;
        for (auto& task : machine)
        {
            std::cout << i++ << ": " << task;
        }
    }

    timeVect = TimeFinder::sort(taskVect);

    std::cout << "\n\nTIMES:\n";
    int lp = 1;
    for (auto& time : timeVect)
    {
        std::cout << lp++ << ": " << time << "\n";
    }

    std::cout << "\n\n";
    std::cout << "NONSORT END TIME: " << timeVect[timeVect.size() - 1] << "\n";

    //SA::sort(taskVect, 5000);
    Tester::test(taskVect, 5000, 10);

    Neh::sort(taskVect2);
    timeVect = TimeFinder::sort(taskVect2);
    std::cout << "\nNEH END TIME: " << timeVect[timeVect.size() - 1] << "\n";

    system("PAUSE");
}
*/

/*
int main()
{
    //TestModule testmodule;
    std::vector<std::vector<Task>> taskVect; //wektor, kt�ry przechowuje taski
    std::vector<int> timeVect;

    Reader reader; //obiekt czytaj�cy z pliku. W konstruktorze mo�esz poda� jego nazw�.
    reader.readFromFileTo(taskVect); //fatkycznie wczytanie z pliku do wektora

    std::cout << "Loaded data:\n";
    for (auto& machine : taskVect)
    {
        int i = 1;
        for (auto& task : machine)
        {
            std::cout << i++ << ": " << task;
        }
        std::cout << "\n";
    }

    timeVect = TimeFinder::sort(taskVect);
    std::cout << "NONSORT CMAX: " << timeVect[timeVect.size() - 1] << "\n";
    Neh::sortMod(taskVect);
    timeVect = TimeFinder::sort(taskVect);
    std::cout << "NEH CMAX: " << timeVect[timeVect.size() - 1] << "\n";

    system("PAUSE");
}
*/

int main()
{
    //TestModule testmodule;
    std::vector<std::vector<Task>> taskVect; //wektor, kt�ry przechowuje taski
    std::vector<int> timeVect;

    Reader reader; //obiekt czytaj�cy z pliku. W konstruktorze mo�esz poda� jego nazw�.
    reader.readFromFileTo(taskVect); //fatkycznie wczytanie z pliku do wektora

    std::cout << "Loaded data:\n";
    for (auto& machine : taskVect)
    {
        int i = 1;
        for (auto& task : machine)
        {
            std::cout << i++ << ": " << task;
        }
        std::cout << "\n";
    }

    std::cout << "\n\n" << "CMax: " << Genetic::sort(taskVect) << "\n";
    std::cout << "INIT CMax: " << TimeFinder::sort(taskVect).back() << "\n";
    /*
    timeVect = TimeFinder::sort(taskVect);
    std::cout << "NONSORT CMAX: " << timeVect[timeVect.size() - 1] << "\n";
    Neh::sortMod(taskVect);
    timeVect = TimeFinder::sort(taskVect);
    std::cout << "NEH CMAX: " << timeVect[timeVect.size() - 1] << "\n";
    */
    system("PAUSE");
}