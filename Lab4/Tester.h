#pragma once
#include <vector>
#include "Task.h"

class Tester
{
public:
    Tester();
    ~Tester();

    static float test(std::vector<std::vector<Task>>& taskVector, float temperature, int amount_of_tests);
};

